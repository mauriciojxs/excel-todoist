# README #

This project is for an Excel integration with TODOist. The file Excel-TODOist.xlsm is the current working version. The sources for VBA can be found in the src folder

### How do I get set up? ###

## For using the tool ##
For using the tool as-is, open the Excel-TODOist.xlsm file, put your TODOist token in the corresponding field in the "Setup" sheet, and refresh the list of projects. If everything works well, you should now be able to select your project from the list.

It is also possible to select a default "label" to be applied to every note created from the spreadsheet.
Select your project, and then use the "Update from TODOist" button on the ribbon to refresh. You should now see the people that share this project on the "People" sheet.

You can now start to create tasks in the corresponding sheet, assigning responsible and due date. You can send this info back to TODOist by clicking the "Send changes to TODOist" button, or by saving the file (and accepting the prompt to update).

## For improving the tool ##
The sheet InternalConfig hosts the internal copy of the data retrieved from TODOist, and normally shouldn't be manually edited. If you want to improve current functionalities, and retrieve some data that currently is not available in the tool, then you probably want to create a new table in this sheet, and sync it to the TODOist calls.
Before any call to the API, you need to initialize the client, and this is done with `call init_todoist(cfg)`, where `cfg` is a global variable that holds the config data (token, project ID, mappings).

There is a general call to sync (sync all) in function `GetTODOist`. There is usually a local call to sync only parts of the data (like `GetProjects`, `GetNotes`...).

Still, there is no online documentation available (Wiki, for instance), but there is some documentation on the code itself. All the functions are presented with their purpose, parameters and return (I think... :wink:).


### Contribution guidelines ###

You are welcome to help building the tool. Take a look in the past issues to see if the topic was already covered. If this is not the case, you are welcome to open a new issue or work from a fork.

One open are is the documentation, also. If you don't think of a feature you could improve, there is plenty of work to organize a Wiki around the code.


### Who do I talk to? ###

For support using the tool, please contact [Mauricio Souza](mailto:mauriciojxs@yahoo.com.br)