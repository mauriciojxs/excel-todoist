Attribute VB_Name = "todoist_Ribbon"
''
' mdRibbon v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Calls for procedures from the ribbon bar
'
' 1. Onload call
' 2. Ribbon calls
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit
Option Private Module

' ============================================= '
' 1. Onload call
' ============================================= '
'
'
' Makes the custom ribbon bar "Logbook" the active one
'
'
' @method rbx_onLoad
' @param {IRibbonUI} ribbon
Public g_rbxUI As IRibbonUI

Public Sub rbx_onLoad( _
    ribbon As IRibbonUI)

    Set g_rbxUI = ribbon
    g_rbxUI.ActivateTab "tabTODOist"

End Sub

'Callback for Invite_Collab onAction
Sub Invite_People( _
    control As IRibbonControl)

    Dim email As String

    If Not isTODOistSet Then
        MsgBox "To set a project ID in TODOist, please go to the Setup tab", vbCritical, "TODOist not set"
        Exit Sub
    End If

    Call Init_TODOist(cfg)
    email = InputBox("Please provide the email address of the new user", "Add new user to project")
    
    If IsEmailValid(email) Then
        'add user
        If AddCollaborator(cfg, email) Then
            'refresh table
            Application.StatusBar = "Waiting for refresh..."
            'safeguard to ensure server processed the request
            Application.Wait Now + TimeSerial(0, 0, 5)
            Call GetTODOist(cfg)
            Call RefreshUsers
            Call ClearStatusBar
        Else
            MsgBox "Add user failed. Please try again later"
        End If
    Else
        MsgBox "Invalid email address, please try again", vbExclamation
    End If

End Sub

'Callback for Delete_Collab onAction
Sub Delete_People( _
    control As IRibbonControl)

    Dim email As String

    On Error GoTo Delete_People_Error

    If Not isTODOistSet Then
        MsgBox "To set a project ID in TODOist, please go to the Setup tab", vbCritical, "TODOist not set"
        Exit Sub
    End If

    Call Init_TODOist(cfg)
    email = InputBox("Please provide the email address of the user to be deleted", "Delete user from project")
    
    If IsEmailValid(email) Then
        'add user
        If DeleteCollaborator(cfg, email) Then
            'refresh table
            Application.StatusBar = "Waiting for refresh..."
            'safeguard to ensure server processed the request
            Application.Wait Now + TimeSerial(0, 0, 5)
            Call GetTODOist(cfg)
            Call RefreshUsers
            Call ClearStatusBar
        Else
            MsgBox "Delete user failed. Please try again later"
        End If
    Else
        MsgBox "Invalid email address, please try again", vbExclamation
    End If

    On Error GoTo 0
    Exit Sub

Delete_People_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure Delete_People of Module todoist_Ribbon"

End Sub

' ============================================= '
' 2. Ribbon calls
' ============================================= '
'
'
' Called from the customized ribbon menu Logbook.
' All these calls have the same @param list
'
'
' @param {IRibbonControl} control
'
'
'

'
' Calls the Archive sub
'
' @method Archive_Tasks()
Public Sub Archive_Tasks( _
    control As IRibbonControl)

    Call Archive

End Sub


'
' Calls the From_TODO_sub sub
'
' @method From_TODO()
Public Sub From_TODO( _
    control As IRibbonControl)

    Call From_TODO_sub

End Sub


'
' Calls the To_TODO_sub sub
'
' @method To_TODO()
Public Sub To_TODO( _
    control As IRibbonControl)

    Call To_TODO_sub

End Sub



'---------------------------------------------------------------------------------------
' Procedure  : Process_Invitations
' Purpose    : Ribbon call to call invitation processing
'
' Parameters : [IRibbonControl] Control - IRibbon call
' Return     : []
'---------------------------------------------------------------------------------------
Public Sub Process_Invitations(control As IRibbonControl)

    On Error GoTo Process_Invitations_Error

    If Not isTODOistSet Then
        MsgBox "To set a project ID in TODOist, please go to the Setup tab", vbCritical, "TODOist not set"
        Exit Sub
    End If

    Call Init_TODOist(cfg)
    Call GetNotifications(cfg, True)
    If Not ProcessInvitations() Then
        Call MsgBox("You have no unprocessed invitations at this time. Please come back later.", vbInformation, _
            "No invitations found")
    End If

    On Error GoTo 0
    Exit Sub

Process_Invitations_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & _
        ") in procedure Process_Invitations of Module todoist_Ribbon"
End Sub



'---------------------------------------------------------------------------------------
' Procedure  : TODOist_Login
' Purpose    : Callback for Login onAction. Setup a TODOist user in the workbook
'
' Parameters : [IRibbonControl] Control - IRibbon call
' Return     : []
'---------------------------------------------------------------------------------------
Sub TODOist_Login(control As IRibbonControl)

    On Error GoTo TODOist_Login_Error
    
    Dim tbl As ListObject
    Set tbl = ThisWorkbook.Sheets("InternalConfig").ListObjects("TblPrjID")
    
    'if there is already a token saved, confirm operation
    If tbl.ListRows(1).Range(tbl.ListColumns("Token").Index).Value <> "" Then
        If MsgBox("There is already one user assigned to this workbook. If you continue, all the tables will be cleared before continuing. Are you sure?", _
            vbOKCancel Or vbExclamation Or vbDefaultButton1, "User overwrite") = vbCancel Then
            Exit Sub
        End If
    End If
    
    'clear tables
    Call ClearTables
    tbl.ListRows(1).Range(tbl.ListColumns("Token").Index).Value = ""
    
    'log in
    Call Init_TODOist(cfg)
    Call GetProjects(cfg)
    
    'save token
    tbl.ListRows(1).Range(tbl.ListColumns("Token").Index) = GetToken(cfg.Project.Client.Authenticator)
    
    On Error GoTo 0
    Exit Sub

TODOist_Login_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure TODOist_Login of Module todoist_Ribbon"
End Sub



'---------------------------------------------------------------------------------------
' Procedure  : TODOist_Logout
' Purpose    :
'
' Parameters :
' Return     : []
'---------------------------------------------------------------------------------------
Sub TODOist_Logout(control As IRibbonControl)

    On Error GoTo TODOist_Logout_Error

    Call Logoff_sub(cfg.Project.Client.Authenticator)
    
    Dim tbl As ListObject
    Set tbl = ThisWorkbook.Sheets("InternalConfig").ListObjects("TblPrjID")
    tbl.ListRows(1).Range(tbl.ListColumns("Token").Index).Value = ""
    Set tbl = Nothing

    On Error GoTo 0
    Exit Sub

TODOist_Logout_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure TODOist_Logout of Module todoist_Ribbon"
End Sub
