Attribute VB_Name = "mdCollecDictHelper"
'---------------------------------------------------------------------------------------
' Project   : Excel_TODOist
' Module    : mdCollecDictHelper
' Author    : Mauricio Souza (mauriciojxs@yahoo.com.br)
' Date      : 2015-09-08
' License   : MIT (http://www.opensource.org/licenses/mit-license.php
' Purpose   : Prints Dictionaries and Collections to the immediate window to help debug
'---------------------------------------------------------------------------------------

Option Private Module
Option Explicit

'---------------------------------------------------------------------------------------
' Procedure  : PrintDictionary
' Purpose    : Prints a Dicitonary to the immediate window
'
' Parameters : [Variant] obj - expected to be a Dictionary, but tests for others
'              [String] Tabul = "", space to be printed before the object, for tabbing
'              [Boolean] PrintNulls = False, allows null fields to print (the field name)
' Return     : []
'---------------------------------------------------------------------------------------
Sub PrintDictionary( _
    obj As Variant, _
    Optional Tabul As String = "", _
    Optional PrintNulls As Boolean = False)

    Dim k As Variant

    On Error GoTo PrintDictionary_Error

    WebHelpers.LogDebug Tabul & "{", "PrintObj"

    If TypeName(obj) = "Dictionary" Then
        For Each k In obj.Keys
            Select Case TypeName(obj(k))
                Case "Collection"
                    WebHelpers.LogDebug Tabul & k, "PrintObj"
                    Call PrintCollection(obj(k), Tabul & Space(3), PrintNulls)
                Case "Dictionary"
                    WebHelpers.LogDebug Tabul & k, "PrintObj"
                    Call PrintDictionary(obj(k), Tabul & Space(3), PrintNulls)
                Case "String", "Double", "Integer", "Boolean"
                    WebHelpers.LogDebug Tabul & k & ": " & obj(k), "PrintObj"
                Case "Null"
                    If PrintNulls Then WebHelpers.LogDebug Tabul & k & ": " & obj(k), "PrintObj"
                Case Else
                    WebHelpers.LogDebug Tabul & "type ukn: " & TypeName(obj), "PrintObj"
            End Select
        Next
    Else
        WebHelpers.LogWarning "error", "PrintObj"
        Select Case TypeName(obj)
            Case "Collection"
                Call PrintCollection(obj, Tabul & Space(3), PrintNulls)
            Case "Dictionary"
                Call PrintDictionary(obj, Tabul & Space(3), PrintNulls)
            Case "String", "Double", "Integer", "Boolean"
                WebHelpers.LogDebug Tabul & TypeName(obj) & ": " & obj, "PrintObj"
            Case "Null"
                If PrintNulls Then WebHelpers.LogDebug Tabul & k & ": " & obj(k), "PrintObj"
            Case Else
                WebHelpers.LogDebug Tabul & "type ukn: " & TypeName(obj), "PrintObj"
        End Select
    End If

    WebHelpers.LogDebug Tabul & "}", "PrintObj"

    On Error GoTo 0
    Exit Sub

PrintDictionary_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure PrintDictionary of Module mdCollecDictHelper"

End Sub





'---------------------------------------------------------------------------------------
' Procedure  : PrintCollection
' Purpose    : Prints a Collection to the immediate window
'
' Parameters : [Variant] obj - expected to be a Collection, but tests for others
'              [String] Tabul = "", space to be printed before the object, for tabbing
'              [Boolean] PrintNulls = False, allows null fields to print (the field name)
' Return     : []
'---------------------------------------------------------------------------------------
Sub PrintCollection( _
    obj As Variant, _
    Optional Tabul As String = "", _
    Optional PrintNulls As Boolean = False)

    Dim k As Variant

    On Error GoTo PrintCollection_Error

    WebHelpers.LogDebug Tabul & "[", "PrintObj"

    If TypeName(obj) = "Collection" Then
        For Each k In obj
            Select Case TypeName(k)
                Case "Collection"
                    Call PrintCollection(k, Tabul & Space(3), PrintNulls)
                Case "Dictionary"
                    Call PrintDictionary(k, Tabul & Space(3), PrintNulls)
                Case "String", "Double", "Integer", "Boolean"
                    WebHelpers.LogDebug Tabul & k, "PrintObj"
                Case "Null"
                    'do nothing
                Case Else
                    WebHelpers.LogDebug Tabul & "type ukn: " & TypeName(k), "PrintObj"
            End Select
        Next
    Else
        WebHelpers.LogWarning "error", "PrintObj"
        Select Case TypeName(obj)
            Case "Collection"
                Call PrintCollection(obj, Tabul & Space(3), PrintNulls)
            Case "Dictionary"
                Call PrintDictionary(obj, Tabul & Space(3), PrintNulls)
            Case "String", "Double", "Integer", "Boolean"
                WebHelpers.LogDebug Tabul & TypeName(obj) & ": " & obj, "PrintObj"
            Case "Null"
                'do nothing
            Case Else
                WebHelpers.LogDebug Tabul & "type ukn: " & TypeName(obj), "PrintObj"
        End Select
    End If
    
    WebHelpers.LogDebug Tabul & "]", "PrintObj"

    On Error GoTo 0
    Exit Sub

PrintCollection_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure PrintCollection of Module mdCollecDictHelper"

End Sub





'---------------------------------------------------------------------------------------
' Procedure  : PrintObj
' Purpose    : Prints an object to the immediate window.
'
' Parameters : [Variant] obj to be printed
'              [String] Tabul = "", space to be printed before the object, for tabbing
'              [Boolean] PrintNulls = False, allows null fields to print (the field name)
' Return     : []
'---------------------------------------------------------------------------------------
Sub PrintObj( _
    obj As Variant, _
    Optional Tabul As String = "", _
    Optional PrintNulls As Boolean = False)
    
    On Error GoTo PrintObj_Error

    Select Case TypeName(obj)
        Case "Dictionary"
            Call PrintDictionary(obj, Tabul, PrintNulls)
        Case "Collection"
            Call PrintCollection(obj, Tabul, PrintNulls)
        Case "Currency", "Decimal", "Long", "Single", "Date", "String", "Double", "Integer", "Boolean"
            WebHelpers.LogDebug Tabul & TypeName(obj) & ": " & obj, "PrintObj"
        Case Else
            WebHelpers.LogDebug Tabul & "Type ukn: " & TypeName(obj), "PrintObj"
    End Select

    On Error GoTo 0
    Exit Sub

PrintObj_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure PrintObj of Module mdCollecDictHelper"
    
End Sub

