Attribute VB_Name = "todoist_TypeDef"
''
' Copy Tasks v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Type definitions and Enum definitions to support the other modules
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit
Option Private Module

'
' used in function user()
'
' @enum {ParamType}
Public Enum ParamType
    email = 1
    Name = 2
    ID = 3
End Enum

'
' used in function label()
'
' @enum {ParamType}
Public Enum LabelReturn
    LabelName = 1
    LabelID = 2
End Enum

'
' used in ConvPriority
'
' @enum {DirectionType}
Public Enum DirectionType
    FromTODOist = 1
    ToTODOist = 2
End Enum


'
' used in FillTask()
'
' @enum {LocalRemote}
Public Enum LocalRemote
    LocalTask = 1
    RemoteTask = 2
End Enum


'
' only for ConfigType def
'
' @type {cfgProjectType}
Private Type cfgProjectType
    Client As New WebClient
    ProjectID As String
    'if you are going to use the same label for all tasks
    LabelID As String
End Type


'
' only for ConfigType def
'
' @type {cfgTaskType}
Private Type cfgTaskType
    Table As ListObject
    clDueDate As Integer
    clStatus As Integer
    ClosedValue As String
    OpenValue As String
    clContent As Integer
    clPriority As Integer
    clResponsible As Integer
    clComments As Integer
    clCommentsID As Integer
    clID As Integer
    clLabel As Integer
End Type


'
' only for ConfigType def
'
' @type {cfgPeopleType}
Private Type cfgPeopleType
    Table As ListObject
    clName As Integer
    clEmail As Integer
    clID As Integer
    clState As Integer
End Type


'
' Type to hold all configurations from the workbook, load at init
'
' @type {ConfigType}
Public Type ConfigType
    Project As cfgProjectType
    Task As cfgTaskType
    People As cfgPeopleType
End Type


'
' not in use
'
' @type {CommentType}
Private Type CommentType
    Date As Date
    ID As String
    Text As String
End Type


'
' to hold Task properties locally or to be passed as parameters
'
' @type {TaskType}
Public Type TaskType
    Text As String
    ID As String
    RespName As String
    Priority As Integer
    DueDate As Date
    Comments() As String
    Closed As Boolean
    label() As String
End Type


'
' to hold Person properties locally or to be passed as parameters
'
' @type {PersonType}
Public Type PersonType
    Name As String
    ID As String
    email As String
End Type
