Attribute VB_Name = "mdGeneral_functions"
''
' General Functions v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' General supporting functions not specific to this spreadsheet
'
' 1. Bubble sort
' 2. Validate email address
' 3. Copy to clipboard
' 4. Get table name
' 5. Autofit text to merged cells
' 6. Split in base 1 arrays
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit
Option Private Module

' ============================================= '
' 1. Bubble sort
' ============================================= '
'
'
' Sorts an array alphabetically
'
'
' @method BubbleSort
' @param {Variant} Array to be sorted
Sub BubbleSort( _
    arr)

    Dim strTemp As String
    Dim i As Long
    Dim j As Long
    Dim lngMin As Long
    Dim lngMax As Long

    lngMin = LBound(arr)
    lngMax = UBound(arr)
    For i = lngMin To lngMax - 1
        For j = i + 1 To lngMax
            If arr(i) > arr(j) Then
                strTemp = arr(i)
                arr(i) = arr(j)
                arr(j) = strTemp
            End If
        Next j
    Next i

End Sub


' ============================================= '
' 2. Validate email address
' ============================================= '
'
'
' Checks if an email address has a valid sintax
'
'
' @method IsEmailValid
' @param {String} Email to be checked
' @return {Boolean} Email is valid
Function IsEmailValid( _
    strEmail As String) _
    As Boolean

    Dim strArray As Variant
    Dim strItem As Variant
    Dim i As Long
    Dim c As String
    Dim blnIsItValid As Boolean

    blnIsItValid = True

    i = Len(strEmail) - Len(Application.Substitute(strEmail, "@", ""))
    If i <> 1 Then IsEmailValid = False: Exit Function
    ReDim strArray(1 To 2)
    strArray(1) = Left(strEmail, InStr(1, strEmail, "@", 1) - 1)
    strArray(2) = Application.Substitute(Right(strEmail, Len(strEmail) - Len(strArray(1))), "@", "")
    For Each strItem In strArray
        If Len(strItem) <= 0 Then
            blnIsItValid = False
            IsEmailValid = blnIsItValid
            Exit Function
        End If
        For i = 1 To Len(strItem)
            c = LCase(Mid(strItem, i, 1))
            If InStr("abcdefghijklmnopqrstuvwxyz_-.", c) <= 0 And Not IsNumeric(c) Then
                blnIsItValid = False
                IsEmailValid = blnIsItValid
                Exit Function
            End If
        Next i
        If Left(strItem, 1) = "." Or Right(strItem, 1) = "." Then
            blnIsItValid = False
            IsEmailValid = blnIsItValid
            Exit Function
        End If
    Next strItem
    If InStr(strArray(2), ".") <= 0 Then
        blnIsItValid = False
        IsEmailValid = blnIsItValid
        Exit Function
    End If
    i = Len(strArray(2)) - InStrRev(strArray(2), ".")
    If i <> 2 And i <> 3 Then
        blnIsItValid = False
        IsEmailValid = blnIsItValid
        Exit Function
    End If
    If InStr(strEmail, "..") > 0 Then
        blnIsItValid = False
        IsEmailValid = blnIsItValid
        Exit Function
    End If
    IsEmailValid = blnIsItValid

End Function



' ============================================= '
' 3. Copy to clipboard
' ============================================= '
'
'
' Copy text to the Windows clipboard.
' VBA Macro using late binding to copy text to clipboard.
' By Justin Kay, 8/15/2014
' Thanks to http://akihitoyamashiro.com/en/VBA/LateBindingDataObject.htm
'
'
' @method CopyText
' @param {String} Text to be copied
Sub CopyText( _
    Text As String)

    Dim MSForms_DataObject As MSForms.DataObject

    Set MSForms_DataObject = CreateObject("new:{1C3B4210-F441-11CE-B9EA-00AA006B1A69}")
    MSForms_DataObject.SetText Text
    MSForms_DataObject.PutInClipboard
    Set MSForms_DataObject = Nothing

End Sub



' ============================================= '
' 4. Get table name
' ============================================= '
'
'
' Returns the name of a table that intersects the
' supplied range
'
'
' @method GetTableName
' @param {Range} Cell or range inside the table
' @return {String} Table name or text "Not a table"
Public Function GetTableName( _
    TableCell As Range) _
    As String

    GetTableName = "Not a table"
    On Error Resume Next
    GetTableName = TableCell.ListObject.Name

End Function




' ============================================= '
' 5. Autofit text to merged cells
' ============================================= '
'
'
' Adjusts cell heigh to fit text, in the case cell
' is merged.
'
'
' @method autofit_merged
' @param {Range} Merged cells to be adjusted
Sub autofit_merged( _
    Target As Range)

    Dim MergeWidth As Single
    Dim cM As Range
    Dim AutoFitRng As Range
    Dim CWidth As Double
    Dim NewRowHt As Double

    Application.ScreenUpdating = False
    On Error Resume Next
    Set AutoFitRng = Target

    With AutoFitRng
        .MergeCells = False
        CWidth = .Cells(1).ColumnWidth
        MergeWidth = 0
        For Each cM In AutoFitRng
            cM.WrapText = True
            MergeWidth = cM.ColumnWidth + MergeWidth
        Next
        'small adjustment to temporary width
        MergeWidth = MergeWidth + AutoFitRng.Cells.Count * 0.66
        .Cells(1).ColumnWidth = MergeWidth
        .EntireRow.AutoFit
        NewRowHt = .RowHeight
        .Cells(1).ColumnWidth = CWidth
        .MergeCells = True
        .RowHeight = NewRowHt
    End With
    Application.ScreenUpdating = True

End Sub



' ============================================= '
' 6. Split in base 1 arrays
' ============================================= '
'
'
' Perform the standard Split functions, but
' returns a Base1 array instead of Base0 as
' in the standard.
'
'
' @method SplitBase1
' @param {String} Text to be split
' @param {String} Delimiter char(s)
' @return {Variant()} Text split in array, starting at 1
Function SplitBase1( _
    ByVal strStringToSplit As String, _
    strDelim As String) _
    As Variant()

    Dim straBase0() As String
    Dim iUbBase0 As Integer
    Dim i As Integer

    If strStringToSplit = "" Then Exit Function

    straBase0 = Split(strStringToSplit, strDelim)
    iUbBase0 = UBound(straBase0)

    ReDim straBase1(1 To iUbBase0 + 1) As Variant

    For i = 0 To iUbBase0
        straBase1(i + 1) = straBase0(i)
    Next i

    SplitBase1 = straBase1

End Function

