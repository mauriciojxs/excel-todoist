Attribute VB_Name = "todoist_Archive_Tasks"
''
' Archive Tasks v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Auxiliary module for archiving closed tasks
'
' 1. Archive tasks
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '
Option Explicit
Option Private Module

' ============================================= '
' 1. Archive tasks
' ============================================= '
'
'
' Called from button click.
' Confirms operation with user. If positive, goes through all
' tasks in the tasklist, checking if:
' - Task is ended
' - Task target date is < specified date range
' If tasks fulfills these requirements, it is moved to the
' table in sheet "Archive".
'
'
' @method Archive
Sub Archive()

    Dim tblSource As ListObject
    Dim tblDest As ListObject
    Dim clsDest As ListColumns
    Dim clsSource As ListColumns
    Dim daysOld As Integer
    Dim srcRows As ListRows
    Dim newRow As ListRow
    Dim counter As Integer
    Dim cMax As Integer
    Dim cl As ListColumn

    On Error GoTo ExitHandler
    daysOld = CInt(InputBox("Delete and copy all tasks finished and older then Number of days?", _
    "Enter number of full days"))
    On Error GoTo 0

    Set tblSource = Sheets("Tasklist").ListObjects("TblTasks")
    Set tblDest = Sheets("Task archive").ListObjects("TblArchive")
    Set clsDest = tblDest.ListColumns
    Set clsSource = tblSource.ListColumns
    Set srcRows = tblSource.ListRows
    cMax = srcRows.Count

    For counter = 0 To (cMax - 1)
        With srcRows(cMax - counter) 'backwards to allow deletions
            If .Range(clsSource("Status").Index) = "E" And .Range(clsSource("Target date").Index) <= (Date - daysOld) Then
                'copy to archive
                Set newRow = tblDest.ListRows.Add
                On Error Resume Next 'in case dest column is not found
                For Each cl In clsSource
                    newRow.Range(clsDest(cl.Name).Index).Value = .Range(cl.Index).Value
                Next
                On Error GoTo 0
                'delete from source
                .Delete
            End If
        End With
    Next

ExitHandler:

End Sub
