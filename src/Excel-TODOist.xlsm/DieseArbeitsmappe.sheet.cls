''
' TODOist client v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Workbook calls for sheets based in the TODOist client framework
'
' 1. BeforeSave
' 2. Workbook Open
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit

' ============================================= '
' 1. BeforeSave
' ============================================= '

''
' Triggered at saving the workbook (manually or at close)
' Checks if the Project is selected in the Setup sheet.
' If positives, asks for confirmation and redirects to the
' To_TODO_sub (upstream refresh)
' If negative, terminates silently
'
'
' @method Workbook_BeforeSave
' @param {Boolean} SaveAsUI
' @param {Boolean} Cancel
Private Sub Workbook_BeforeSave( _
    ByVal SaveAsUI As Boolean, _
    Cancel As Boolean)

    'save token
    If GetToken(cfg.Project.Client.Authenticator) <> "" Then
        Dim tbl As ListObject
        Set tbl = ThisWorkbook.Sheets("InternalConfig").ListObjects("TblPrjID")
        tbl.ListRows(1).Range(tbl.ListColumns("Token").Index) = GetToken(cfg.Project.Client.Authenticator)
        Set tbl = Nothing
    End If

    If isTODOistSet Then
        Call Init_TODOist(cfg)
    Else
        Exit Sub
    End If
    If MsgBox("Save changes to TODOist?", vbYesNo, "Update confirmation") = vbYes Then
        Call To_TODO_sub(True)
    End If

End Sub

' ============================================= '
' 2. Workbook Open
' ============================================= '

''
' Triggered at opening the workbook
' Checks if the Project is selected in the Setup sheet.
' If positives, asks for confirmation and redirects to the
' From_TODO_sub (downstream refresh)
' If negative, terminates silently
'
'
' @method Workbook_Open
Private Sub Workbook_Open()

    If isTODOistSet Then
        Call Init_TODOist(cfg)
    Else
        Exit Sub
    End If
    If MsgBox("Update from TODOist?", vbYesNo, "Update confirmation") = vbYes Then
        Call From_TODO_sub
    End If

End Sub