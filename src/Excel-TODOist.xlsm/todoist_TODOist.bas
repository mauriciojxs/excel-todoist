Attribute VB_Name = "todoist_TODOist"
''
' mdTODOist v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Calls to initialize TODOist client, refresh local copies of the
' database, or add/update data in TODOist API. Based in TODOist API v6
'
' 1. Initialize TODOist client
' 2. Project routines
' 3. Tasks routines
' 4. Collaborators routines
' 5. Labels routines
' 6. Notes routines
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Code to create columns in a table from the dictionary keys '
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'For Each a In Result.Keys
'    If a = "Collaborators" Then
'        For Each b In Result(a)
'            Debug.Print TypeName(b)
'                For Each c In b.Keys
'                    Set newcolumn = clCollab.Add
'                    newcolumn.Name = c
'                Next
'        Next
'    End If
'Next
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Option Explicit
Option Private Module

Public cfg As ConfigType


' ============================================= '
' 1. Initialize TODOist client
' ============================================= '
'
'
' Initializes the TODOist client and set the global
' variables necessary in the project
'
'
'
' @method Init_TODOist
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Enable logging to immediate window
Sub Init_TODOist( _
    ByRef Config As ConfigType, _
    Optional ByVal Logging As Boolean = False)
    
    Dim SheetName As String
    Dim TableName As String
    Dim tempString As String
        
    'set TRUE to enable debbuging in the immediate window
    WebHelpers.EnableLogging = Logging

    'API Base URL provided by TODOist
    Config.Project.Client.BaseUrl = "https://todoist.com/API/v6/"

    'Project parameters
    With Sheets("InternalConfig").ListObjects("TblPrjID")
        Config.Project.ProjectID = .ListColumns("Project_ID").DataBodyRange
        
        'uncomment next line to use the same label for all tasks
        'Config.Project.LabelID = .ListColumns("Label_ID").DataBodyRange
        
        'Config.Project.Token = .ListColumns("Token").DataBodyRange
    End With

    'Tasks location
    With Sheets("InternalConfig").ListObjects("TblCfgTasks_Active")
        SheetName = .ListColumns("TblTasks_Location").DataBodyRange
        TableName = .ListColumns("TblTasks_Name").DataBodyRange
        Set Config.Task.Table = Sheets(SheetName).ListObjects(TableName)
        tempString = .ListColumns("Task_DueDate").DataBodyRange
        Config.Task.clDueDate = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_Status").DataBodyRange
        Config.Task.clStatus = Config.Task.Table.ListColumns(tempString).Index
        Config.Task.ClosedValue = .ListColumns("Task_StatusClosed").DataBodyRange
        Config.Task.OpenValue = .ListColumns("Task_StatusOpen").DataBodyRange
        tempString = .ListColumns("Task_Content").DataBodyRange
        Config.Task.clContent = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_Priority").DataBodyRange
        Config.Task.clPriority = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_ResponsibleName").DataBodyRange
        Config.Task.clResponsible = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_Comments").DataBodyRange
        Config.Task.clComments = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_CommentsID").DataBodyRange
        Config.Task.clCommentsID = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_ID").DataBodyRange
        Config.Task.clID = Config.Task.Table.ListColumns(tempString).Index
        tempString = .ListColumns("Task_Label").DataBodyRange
        Config.Task.clLabel = Config.Task.Table.ListColumns(tempString).Index
    End With

    'People location
    With Sheets("InternalConfig").ListObjects("TblCfgPeople_Active")
        SheetName = .ListColumns("TblPeople_Location").DataBodyRange
        TableName = .ListColumns("TblPeople_Name").DataBodyRange
        Set Config.People.Table = Sheets(SheetName).ListObjects(TableName)
        tempString = .ListColumns("People_Name").DataBodyRange
        Config.People.clName = Config.People.Table.ListColumns(tempString).Index
        tempString = .ListColumns("People_Email").DataBodyRange
        Config.People.clEmail = Config.People.Table.ListColumns(tempString).Index
        tempString = .ListColumns("People_ID").DataBodyRange
        Config.People.clID = Config.People.Table.ListColumns(tempString).Index
        tempString = .ListColumns("People_State").DataBodyRange
        Config.People.clState = Config.People.Table.ListColumns(tempString).Index
    End With

    'Authenticator
    If Config.Project.Client.Authenticator Is Nothing Then
        Dim tblApp As ListObject
        Dim tblPrj As ListObject
        Dim Auth As New TodoistAuthenticator
        
        Set tblApp = ThisWorkbook.Sheets("InternalConfig").ListObjects("TblAppSettings")
        Set tblPrj = ThisWorkbook.Sheets("InternalConfig").ListObjects("TblPrjID")
        Auth.Setup ClientId:=tblApp.ListRows(1).Range(tblApp.ListColumns("App ID").Index), _
            ClientSecret:=tblApp.ListRows(1).Range(tblApp.ListColumns("App Secret").Index), _
            RedirectURL:=tblApp.ListRows(1).Range(tblApp.ListColumns("RedirectURL").Index)
        Auth.Scope = "data:read_write"
        Set Config.Project.Client.Authenticator = Auth
        If tblPrj.ListRows(1).Range(tblPrj.ListColumns("Token").Index).Value <> "" Then
            Auth.Token = tblPrj.ListRows(1).Range(tblPrj.ListColumns("Token").Index).Value
        End If
    End If

End Sub

Function isTODOistSet() As Boolean

    If Sheets("InternalConfig").ListObjects("TblPrjID").DataBodyRange(Sheets("InternalConfig").ListObjects("TblPrjID").ListColumns.Item("Project_ID").Index) = "" Then
        isTODOistSet = False
    Else
        isTODOistSet = True
    End If

End Function


' ============================================= '
' 2. Refresh local tables
' ============================================= '
'
'
' Handles project information to / from TODOist
'
'
'

'
' Refresh internal table of projects from TODOist
'
' reference: https://developer.todoist.com/#retrieve-data
'
' @method GetTODOist
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetTODOist( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim seq_no As String
    Dim seq_no_global As String
    Dim ws As Worksheet
    Dim Result As Dictionary
    Dim d As Variant
    Dim ErrorList() As Variant
    Dim ErrorCount As Integer
    Dim i As Integer
    Dim Item As ListRow
    
    On Error GoTo GetTODOist_Error

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(7).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(7).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """all""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        For i = 1 To ws.ListObjects("TblSeq").ListRows.Count
            ws.ListObjects("TblSeq").ListRows(i).Range(2) = seq_no
            ws.ListObjects("TblSeq").ListRows(i).Range(3) = seq_no_global
        Next

        Set Result = ParseJson(Response.Content)
        
        GetTODOist = True
        
        If reset Then
            On Error Resume Next
            ws.ListObjects("TblProjects").DataBodyRange.Delete
            ws.ListObjects("TblItems").DataBodyRange.Delete
            ws.ListObjects("TblLabels").DataBodyRange.Delete
            ws.ListObjects("tblNotes").DataBodyRange.Delete
            ws.ListObjects("tblFilters").DataBodyRange.Delete
            ws.ListObjects("TblReminders").DataBodyRange.Delete
            ws.ListObjects("TblCollaborators").DataBodyRange.Delete
            ws.ListObjects("tblCollaboratorStates").DataBodyRange.Delete
            For Each Item In ws.ListObjects("tblNotifications").ListRows
                ws.ListObjects("tbl" & Item.Range).DataBodyRange.Delete
            Next
            On Error GoTo GetTODOist_Error
        End If
        
        'for each project returned
        For Each d In Result.Keys
            WebHelpers.LogDebug (d), "GetTODOist"
            Select Case UCase(d)
                Case "PROJECTS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "TblProjects")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Projects"
                    End If
                Case "ITEMS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "TblItems")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Items"
                    End If
                Case "LABELS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "TblLabels")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Labels"
                    End If
                Case "NOTES"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "tblNotes")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Notes"
                    End If
                Case "FILTERS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "tblFilters")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Filters"
                    End If
                Case "REMINDERS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "TblReminders")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Reminders"
                    End If
                Case "LOCATIONS"
                    'this is a collection of location name, longitude, latitude. nothing to be done
                Case "USER"
                    If TypeName(Result(d)) = "Dictionary" Then
                        PrintObj Result(d), Space(3)
                        Call InterpretUserInfo(Result(d))
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "User"
                    End If
                Case "COLLABORATORS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "TblCollaborators")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "Collaborators"
                    End If
                Case "COLLABORATORSTATES"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call InterpretCollaboratorStates(Result(d))
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "CollaboratorStates"
                    End If
                Case "LIVENOTIFICATIONS"
                    If TypeName(Result(d)) = "Collection" Then
                        PrintObj Result(d), Space(3)
                        Call Interpret(Result(d), "tblLiveNotifications", "seq_no")
                    Else
                        ReDim Preserve ErrorList(1 To ErrorCount + 1)
                        ErrorCount = ErrorCount + 1
                        ErrorList(ErrorCount) = "LiveNotifications"
                    End If
                Case Else
                    PrintObj Result(d), Space(3)
            End Select
        Next
    Else
        GetTODOist = False
        WebHelpers.LogError Response.StatusDescription, "GetTODOist", Response.StatusCode
        WebHelpers.LogError Response.Content, "GetTODOist"
    End If
    
    If ErrorCount > 0 Then
        For i = 1 To ErrorCount
            WebHelpers.LogError Space(3) & ErrorList(i), "GetTODOist calls"
        Next
    End If

    On Error GoTo 0
    Exit Function

GetTODOist_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure GetTODOist of Module todoist_TODOist"

End Function



' ============================================= '
' 2. Project routines
' ============================================= '
'
'
' Handles project information to / from TODOist
'
'
'

'
' Call TODOist for a project refresh
'
' reference: https://developer.todoist.com/#retrieve-data
'
' @method GetProjects
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetProjects( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim seq_no As String
    Dim seq_no_global As String
    Dim ws As Worksheet

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(2).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(2).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", "22df79eea3e9118665934a11d583b40fab9876f4"
    
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """projects""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then
    
        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(2).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(2).Range(3) = seq_no_global

        GetProjects = True
        
        Call Interpret(Response.Data("Projects"), "TblProjects")
    Else
        GetProjects = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function




' ============================================= '
' 3. Tasks routines
' ============================================= '
'
'
' Handles tasks information to / from TODOist
'
'
'

'
' Call TODOist for a Tasks refresh
'
' reference: https://developer.todoist.com/#retrieve-data
'
' @method GetTasks
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetTasks( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim ws As Worksheet
    Dim seq_no As String
    Dim seq_no_global As String

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(3).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(3).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """items""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)
    
    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then
        GetTasks = True

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(3).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(3).Range(3) = seq_no_global

        Call Interpret(Response.Data("Items"), "TblItems")
        
    Else
        GetTasks = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function

'
' Add a task to TODOist
'
' reference: https://developer.todoist.com/#add-an-item
'
' @method AddTask
' @param {ConfigType} Configuration variable
' @param {TaskType} Task to be added
' @return {String} TaskID if sucessful, vbNullString if not
Function AddTask( _
    ByRef Config As ConfigType, _
    ByRef TaskLocal As TaskType) _
    As String

    Dim temp_id As String
    Dim Response As WebResponse
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    Dim TryCounter As Integer
    
    TryCounter = 0

TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", 0
    Request.AddQuerystringParam "seq_no_global", 0

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "item_add"
    Cmd.Add "temp_id", temp_id
    Cmd.Add "uuid", temp_id
    
    
    Args.Add "content", UrlEncode(TaskLocal.Text)
    Args.Add "project_id", Config.Project.ProjectID
    If TaskLocal.DueDate <> 0 Then Args.Add "date_string", Format(TaskLocal.DueDate, "yyyy-mm-dd")
    If TaskLocal.Priority <> 0 Then Args.Add "priority", ConvPriority(TaskLocal.Priority, ToTODOist)
    If TaskLocal.RespName <> "" Then Args.Add "responsible_uid", user(TaskLocal.RespName, Name)
    
    If Config.Project.LabelID = "" Then
        Args.Add "labels", "[" & Join(TaskLocal.label, ",") & "]"
    Else
        Args.Add "labels", "[" & Config.Project.LabelID & "]"
    End If
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        
        'if we got "Already Processed" error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    AddTask = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                TaskLocal.ID = Response.Data("TempIdMapping")(temp_id)
                AddTask = TaskLocal.ID
            End If
        End If
    Else
        Debug.Print Response.StatusCode
        AddTask = vbNullString
    End If

End Function

'
' Update one single task in TODOist
'
' reference: https://developer.todoist.com/#update-an-item
'
' @method UpdateTask
' @param {ConfigType} Configuration variable
' @param {TaskType} Task to be updated
' @return {Boolean} TRUE if update was sucessful
Function UpdateTask( _
    ByRef Config As ConfigType, _
    ByRef Task As TaskType) _
    As Boolean

    Dim temp_id As String
    Dim Response As WebResponse
    Dim TryCounter As Integer
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String

    TryCounter = 0

TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", 0
    Request.AddQuerystringParam "seq_no_global", 0

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "item_update"
    Cmd.Add "temp_id", temp_id
    Cmd.Add "uuid", temp_id
    
    Args.Add "id", Task.ID
    If Task.DueDate <> 0 Then Args.Add "date_string", Format(Task.DueDate, "yyyy-mm-dd") _
        Else Args.Add "date_string", ""
    Args.Add "priority", ConvPriority(Task.Priority, ToTODOist)
    Args.Add "responsible_uid", user(Task.RespName, Name)
    If Task.Closed Then
        Args.Add "checked", 1
    Else
        Args.Add "checked", 0
        Args.Add "in_history", 0
        Args.Add "is_archived", 0
    End If
    
    If Config.Project.LabelID = "" Then
        Args.Add "labels", "[" & Join(Task.label, ",") & "]"
    Else
        Args.Add "labels", "[" & Config.Project.LabelID & "]"
    End If
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then

        'if we got error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                'already processed
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                'to many requests in 1 minute
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    Debug.Print Response.Data("SyncStatus")(temp_id)("error_code")
                    UpdateTask = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                UpdateTask = True
            Else
                UpdateTask = False
            End If
        End If
    Else
        UpdateTask = False
    End If

End Function


' Close (complete) a list of tasks in TODOist
'
' reference: https://developer.todoist.com/#complete-items
'
' commands='[{"type": "item_complete", "uuid": "a74bfb5c-5f1d-4d14-baea-b7415446a871",
' "args": {"project_id": 128501470, "ids": [33548400]}}]'
'
'
' @method CloseTasks
' @param {ConfigType} Configuration variable
' @param {String()} Tasks to be closed
' @return {Boolean} TRUE if close was sucessful
Function CloseTasks( _
    ByRef Config As ConfigType, _
    ByRef tasks() As String) _
    As Boolean

    Dim temp_id As String
    Dim Response As WebResponse
    Dim TryCounter As Integer
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    Dim i As Integer

    TryCounter = 0

TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "item_complete"
    Cmd.Add "uuid", temp_id
    
    Args.Add "project_id", Config.Project.ProjectID
    Args.Add "ids", "[" & Join(tasks, ",") & "]"
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            If Response.Data("SyncStatus")(temp_id)("error_code") = "" Then
                CloseTasks = True
                For i = LBound(tasks) To UBound(tasks)
                    If Response.Data("SyncStatus")(temp_id)(tasks(i)) <> "ok" Then
                        CloseTasks = False
                        Exit Function
                    End If
                Next
            Else
                Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                    'already processed
                    Case "37"
                        If TryCounter < 4 Then
                            GoTo TryAgain
                        Else
                            Debug.Print "Too many tries, giving up..."
                        End If
                    'to many requests in 1 minute
                    Case "35"
                        Debug.Print "Too many requests, waiting 20s"
                        Application.Wait (Now + TimeValue("0:00:20"))
                        GoTo TryAgain
                    Case Default
                        Debug.Print Response.Data("SyncStatus")(temp_id)("error_code")
                        CloseTasks = False
                End Select
            End If
        Else
            CloseTasks = False
        End If
    Else
        Debug.Print Response.StatusCode
        CloseTasks = False
    End If

End Function

'
' Delete a list of tasks in TODOist
'
' reference: https://developer.todoist.com/#delete-items
'
' https://todoist.com/API/v6/sync \
'    -d token=0123456789abcdef0123456789abcdef01234567 \
'    -d commands='[{"type": "item_delete", "uuid": "f8539c77-7fd7-4846-afad-3b201f0be8a5", "args": {"ids": [33548400]}}]'
' { ...
'  "SyncStatus": {"f8539c77-7fd7-4846-afad-3b201f0be8a5": {"33548400": "ok"}},
'
'
' @method DeleteTasks
' @param {ConfigType} Configuration variable
' @param {String()} Tasks to be deleted
' @return {Boolean} TRUE if delete was sucessful
Function DeleteTasks( _
    ByRef Config As ConfigType, _
    ByRef tasks() As String) _
    As Boolean

    Dim temp_id As String
    Dim Response As WebResponse
    Dim TryCounter As Integer
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    Dim i As Integer

    TryCounter = 0

TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "item_delete"
    Cmd.Add "uuid", temp_id
    
    Args.Add "ids", "[" & Join(tasks, ",") & "]"
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            If Response.Data("SyncStatus")(temp_id)("error_code") = "" Then
                DeleteTasks = True
                For i = LBound(tasks) To UBound(tasks)
                    If Response.Data("SyncStatus")(temp_id)(tasks(i)) <> "ok" Then
                        DeleteTasks = False
                        Exit Function
                    End If
                Next
            Else
                Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                    'already processed
                    Case "37"
                        If TryCounter < 4 Then
                            GoTo TryAgain
                        Else
                            Debug.Print "Too many tries, giving up..."
                        End If
                    'to many requests in 1 minute
                    Case "35"
                        Debug.Print "Too many requests, waiting 20s"
                        Application.Wait (Now + TimeValue("0:00:20"))
                        GoTo TryAgain
                    Case Default
                        Debug.Print Response.Data("SyncStatus")(temp_id)("error_code")
                        DeleteTasks = False
                End Select
            End If
        Else
            DeleteTasks = False
        End If
    Else
        Debug.Print Response.StatusCode
        DeleteTasks = False
    End If

End Function
  

' ============================================= '
' 4. Collaborators routines
' ============================================= '
'
'
' Handles tasks information to / from TODOist
'
'
'

'
' Calls TODOist for a Collaborators refresh
'
' reference: https://developer.todoist.com/#retrieve-data
'
' @method GetCollaborators
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetCollaborators( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim ws As Worksheet
    Dim seq_no As String
    Dim seq_no_global As String

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    'to return all data instead of refresh
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(1).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(1).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """collaborators""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then
        GetCollaborators = True

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(1).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(1).Range(3) = seq_no_global

        Call Interpret(Response.Data("Collaborators"), "TblCollaborators")
        
    Else
        GetCollaborators = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function


'
' Call TODOist for a refresh of the User table, that holds current user
' parameters
'
' reference: https://developer.todoist.com/#retrieve-data
'
' @method GetUserInfo
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetUserInfo( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim seq_no As String
    Dim seq_no_global As String
    Dim ws As Worksheet

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    'refresh all the data, not only new one
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(5).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(5).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """user""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(5).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(5).Range(3) = seq_no_global

        GetUserInfo = True

        Call InterpretUserInfo(Response.Data("User"))
        
    Else
        Call MsgBox("TODOist page returned the following message:" _
            & vbCrLf & Response.Data("error") _
            & vbCrLf & "Please try again later." _
            , vbExclamation Or vbDefaultButton1, "Error")
        GetUserInfo = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function

' Refresh internal table of User data from a dictionary of parameters
'
' @method InterpretUserInfo
' @param {Dictionary} UserInfo
' @return {Boolean} Update was sucessful
Function InterpretUserInfo( _
    user As Dictionary) _
    As Boolean

    Dim tblUserInfo As ListObject
    Dim rowUser As ListRow
    Dim cl As ListColumn

    Set tblUserInfo = Sheets("InternalConfig").ListObjects("TblUserInfo")

    'set line 1 as rowUser
    If tblUserInfo.ListRows.Count < 1 Then tblUserInfo.ListRows.Add
    Set rowUser = tblUserInfo.ListRows(1)

    'fill data
    For Each cl In tblUserInfo.ListColumns
        If (cl.Name) = "tz_offset" Then
            rowUser.Range(cl.Index) = user(cl.Name)(2)
        Else
            rowUser.Range(cl.Index) = user(cl.Name)
        End If
    Next

End Function


'
' Invite a collaborator to TODOist
'
' reference: https://developer.todoist.com/#share-a-project
'
' $ curl https://todoist.com/API/v6/sync \
'    -d token=0123456789abcdef0123456789abcdef01234567 \
'    -d commands='[{"type": "share_project", "temp_id": "854be9cd-965f-4ddd-a07e-6a1d4a6e6f7a",
' "uuid": "fe6637e3-03ce-4236-a202-8b28de2c8372", "args": {"project_id": "128501470", "message": "",
' "email": "you@example.com"}}]'
' { ...
'  "SyncStatus": {"fe6637e3-03ce-4236-a202-8b28de2c8372": "ok"},
'  ... }
'
'
' @method AddCollaborator
' @param {ConfigType} Configuration variable
' @param {String} Collaborator email
' @return {String} New collaborator ID number
Function AddCollaborator( _
    ByRef Config As ConfigType, _
    ByVal email As String) _
    As String

    Dim temp_id As String
    Dim Response As WebResponse
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    
    Dim TryCounter As Integer
    
    TryCounter = 0
    
TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "share_project"
    Cmd.Add "temp_id", temp_id
    Cmd.Add "uuid", temp_id
    
    Args.Add "project_id", Config.Project.ProjectID
    Args.Add "email", email
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        
        'if we got "Already Processed" error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    AddCollaborator = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                AddCollaborator = Response.Data("TempIdMapping")(temp_id)
            End If
        End If
    Else
        AddCollaborator = False
    End If

End Function



'
' Delete a collaborator from a project in TODOist
'
' reference: https://developer.todoist.com/#delete-a-collaborator
'
' $ curl https://todoist.com/API/v6/sync \
'    -d token=0123456789abcdef0123456789abcdef01234567 \
'    -d commands='[{"type": "delete_collaborator", "uuid": "0ae55ac0-3b8d-4835-b7c3-59ba30e73ae4",
' "args": {"project_id": 128501470, "email": "you@example.com"}}]'
' { ...
'  "SyncStatus": {"0ae55ac0-3b8d-4835-b7c3-59ba30e73ae4": "ok"},
'  ... }
'
'
' @method DeleteCollaborator
' @param {ConfigType} Configuration variable
' @param {String} Collaborator email
' @return {Boolean} Delete was sucessful
Function DeleteCollaborator( _
    ByRef Config As ConfigType, _
    ByVal email As String) _
    As Boolean

    Dim temp_id As String
    Dim Response As WebResponse
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    
    Dim TryCounter As Integer
    
    TryCounter = 0
    
TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "delete_collaborator"
    Cmd.Add "uuid", temp_id
    
    Args.Add "project_id", Config.Project.ProjectID
    Args.Add "email", email
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        
        'if we got "Already Processed" error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    DeleteCollaborator = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                DeleteCollaborator = True
            End If
        End If
    Else
        DeleteCollaborator = False
    End If

End Function


' ============================================= '
' 5. Labels routines
' ============================================= '
'
'
' Handles labels information to / from TODOist
'
'
'

'
' Calls TODOist for a refresh of labels
'
' @method GetLabels
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetLabels( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Request As New WebRequest
    Dim Response As WebResponse
    Dim ws As Worksheet
    Dim seq_no As String
    Dim seq_no_global As String

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    'to return all data instead of refresh
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(4).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(4).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """labels""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)
    
    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then
        GetLabels = True

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")
        
        ws.ListObjects("TblSeq").ListRows(4).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(4).Range(3) = seq_no_global
        

        Call Interpret(Response.Data("Labels"), "TblLabels")
    Else
        GetLabels = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function



' ============================================= '
' 6. Notes routines
' ============================================= '
'
'
' Handles notes information to / from TODOist
'
'
'

'
' Calls TODOist for a refresh of notes
'
' @method GetNotes
' @param {ConfigType} Configuration variable
' @param {Boolean = False} Reset counter so that all items are returned, instead of differential update
' @return {Boolean} Update was sucessful
Function GetNotes( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Request As New WebRequest
    Dim Response As WebResponse
    Dim seq_no As String
    Dim seq_no_global As String
    Dim ws As Worksheet

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    'refresh all the data, not only new one
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(6).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(6).Range(3)
    End If
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """notes""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)
    
    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(6).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(6).Range(3) = seq_no_global

        GetNotes = True

        Call Interpret(Response.Data("Notes"), "TblNotes")
    Else
        GetNotes = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

End Function


'
' Add a note to TODOist
'
' @method AddNote
' @param {ConfigType} Configuration variable
' @param {String} Text of the note
' @param {String} Task ID to add the note to
' @return {String} Note ID, or vbNullString if unsucessful
Function AddNote( _
    ByRef Config As ConfigType, _
    ByVal NoteText As String, _
    ByVal TaskID As String) _
    As String

    Dim temp_id As String
    Dim Response As WebResponse
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    
    Dim TryCounter As Integer
    
    TryCounter = 0
    
TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", 0
    Request.AddQuerystringParam "seq_no_global", 0

    'this is the specific part
    temp_id = GenGuid()
    
    Cmd.Add "type", "note_add"
    Cmd.Add "temp_id", temp_id
    Cmd.Add "uuid", temp_id
    
    Args.Add "content", UrlEncode(NoteText)
    Args.Add "item_id", TaskID
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        
        'if we got "Already Processed" error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    AddNote = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                AddNote = Response.Data("TempIdMapping")(temp_id)
            End If
        End If
    Else
        AddNote = vbNullString
    End If

End Function



' Refresh internal table tblCollaboratorStates from a collection of parameters
'
' @method Interpret
' @param {Collection} Items
' @return {Boolean} Update was sucessful
Function InterpretCollaboratorStates(Items As Collection) As Boolean

    Dim tblItems As ListObject
    Dim rowItem As ListRow
    Dim row As ListRow
    Dim cl As ListColumn
    Dim Item As Variant

    On Error GoTo InterpretCollaboratorStates_Error

    Set tblItems = Sheets("InternalConfig").ListObjects("tblCollaboratorStates")
    
    'for each item returned
    For Each Item In Items
        If Not TypeName(Item) = "Collection" Then

            'check if existing, and set rowItem
            Set rowItem = Nothing
            For Each row In tblItems.ListRows
                If row.Range(tblItems.ListColumns("user_id").Index) = Item("user_id") And _
                    row.Range(tblItems.ListColumns("project_id").Index) = Item("project_id") Then
                    Set rowItem = row
                    Exit For
                End If
            Next
    
            'if not existing, create new row
            If rowItem Is Nothing Then
                Set rowItem = tblItems.ListRows.Add
            End If
    
            'fill data
            For Each cl In tblItems.ListColumns
                rowItem.Range(cl.Index) = Item(cl.Name)
            Next
        
        End If
    Next

    On Error GoTo 0
    Exit Function

InterpretCollaboratorStates_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & _
        ") in procedure InterpretCollaboratorStates of Module todoist_TODOist"

End Function


'---------------------------------------------------------------------------------------
' Procedure  : ReplyInvitation
' Purpose    : Accept or reject an invitation to share a project or to join a business account
'
' Parameters : [ConfigType] Configuration object
'              [String] Invitation ID
'              [String] Invitation Secret
'              [Boolean] Invitation is accepted
'              [Boolean] Invitation is for a business account
' Return     : [Boolean] Processing status
'---------------------------------------------------------------------------------------
Function ReplyInvitation( _
    ByRef Config As ConfigType, _
    ByVal InvitationID As String, _
    ByVal InvitationSecret As String, _
    ByVal Accepted As Boolean, _
    ByVal isBizInvitation As Boolean) _
    As Boolean

    Dim temp_id As String
    Dim Response As WebResponse
    Dim Request As WebRequest
    Dim Cmd As Dictionary
    Dim Args As Dictionary
    Dim CommandStr As String
    Dim biz As String
    
    Dim TryCounter As Integer
    
    On Error GoTo ReplyInvitation_Error

    TryCounter = 0
    
    If isBizInvitation Then
        biz = "biz_"
    Else
        biz = ""
    End If
    
TryAgain:
    TryCounter = TryCounter + 1
    Set Request = New WebRequest
    Set Cmd = New Dictionary
    Set Args = New Dictionary
    
    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token

    'this is the specific part
    temp_id = GenGuid()
    
    If Accepted Then
        Cmd.Add "type", biz & "accept_invitation"
    Else
        Cmd.Add "type", biz & "reject_invitation"
    End If
    Cmd.Add "uuid", temp_id
    
    Args.Add "invitation_id", InvitationID
    Args.Add "invitation_secret", InvitationSecret
    
    Cmd.Add "args", Args
    
    CommandStr = WebHelpers.ConvertToFormat(Cmd, Request.RequestFormat, Request.CustomRequestFormat)
    Request.AddQuerystringParam "commands", "[" & CommandStr & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)

    If Response.StatusCode = WebStatusCode.Ok Then
        
        'if we got "Already Processed" error
        If TypeName(Response.Data("SyncStatus")(temp_id)) = "Dictionary" Then
            Select Case Response.Data("SyncStatus")(temp_id)("error_code")
                Case "37"
                    If TryCounter < 4 Then
                        GoTo TryAgain
                    Else
                        Debug.Print "Too many tries, giving up..."
                    End If
                Case "35"
                    Debug.Print "Too many requests, waiting 20s"
                    Application.Wait (Now + TimeValue("0:00:20"))
                    GoTo TryAgain
                Case Default
                    ReplyInvitation = False
            End Select
        Else
            If Response.Data("SyncStatus")(temp_id) = "ok" Then
                ReplyInvitation = True
            End If
        End If
    Else
        ReplyInvitation = False
    End If

    On Error GoTo 0
    Exit Function

ReplyInvitation_Error:

    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure ReplyInvitation of Module todoist_TODOist"

End Function

'---------------------------------------------------------------------------------------
' Procedure  : GetNotifications
' Purpose    : Retrieve LiveNotifications from TODOist
'
' Parameters : [ConfigType] Project configuration parameters
'              [Boolean] Reset = False, Retrieve all data (instead of just updates)
' Return     : [Boolean] True if there were notifications to process
'---------------------------------------------------------------------------------------
Function GetNotifications( _
    ByRef Config As ConfigType, _
    Optional ByVal reset As Boolean = False) _
    As Boolean

    Dim Response As WebResponse
    Dim Request As New WebRequest
    Dim ws As Worksheet
    Dim seq_no As String
    Dim seq_no_global As String

    On Error GoTo GetNotifications_Error

    Set ws = ThisWorkbook.Sheets("InternalConfig")
    
    If reset Then
        seq_no = 0
        seq_no_global = 0
    Else
        seq_no = ws.ListObjects("TblSeq").ListRows(8).Range(2)
        seq_no_global = ws.ListObjects("TblSeq").ListRows(8).Range(3)
    End If

    'this is the common part
    Request.Method = WebMethod.Httpget
    Request.Format = WebFormat.Json
    Request.Resource = "sync"
    'Request.AddQuerystringParam "token", Config.Project.Token
    Request.AddQuerystringParam "seq_no", seq_no
    Request.AddQuerystringParam "seq_no_global", seq_no_global

    'this is the specific part
    Request.AddQuerystringParam "resource_types", "[" & """live_notifications""" & "]"

    'execute
    Set Response = Config.Project.Client.Execute(Request)
    
    'if request if sucessful
    If Response.StatusCode = WebStatusCode.Ok Then

        seq_no = Response.Data("seq_no")
        seq_no_global = Response.Data("seq_no_global")

        ws.ListObjects("TblSeq").ListRows(8).Range(2) = seq_no
        ws.ListObjects("TblSeq").ListRows(8).Range(3) = seq_no_global

        GetNotifications = Interpret(Response.Data("LiveNotifications"), "TblLiveNotifications", "seq_no")
        
    Else
        GetNotifications = False
        Debug.Print "Error: " & Response.StatusDescription
        Debug.Print Response.Content
    End If

    On Error GoTo 0
    Exit Function

GetNotifications_Error:

    GetNotifications = False
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure GetNotifications of Module todoist_TODOist"

End Function



'---------------------------------------------------------------------------------------
' Procedure  : Interpret
' Purpose    : Fills the internal tables from a Collection
'
' Parameters : [Collection] Items to be passed to the table
'              [String] Name of the table to populate
'              [String] id_field = "id", key column of the table
' Return     : [Boolean]
'---------------------------------------------------------------------------------------
Function Interpret( _
    Items As Collection, _
    tblName As String, _
    Optional id_field As String = "id") _
    As Boolean

    Dim tblItems As ListObject
    Dim rowItem As ListRow
    Dim row As ListRow
    Dim cl As ListColumn
    Dim Item As Variant
    Dim subItem As Variant
    Dim subItemList() As String
    Dim i As Integer
    Dim Col As Collection

    'On Error GoTo Interpret_Error
    On Error GoTo 0

    Set tblItems = Sheets("InternalConfig").ListObjects(tblName)

    'for each item returned
    For Each Item In Items
        If TypeName(Item) = "Dictionary" Then
            
            'check if existing, and set rowItem
            On Error GoTo NoID
            
            Set rowItem = Nothing
            For Each row In tblItems.ListRows
                If row.Range(tblItems.ListColumns(id_field).Index) = Item(id_field) Then
                    Set rowItem = row
                    Exit For
                End If
            Next
NoID:
            On Error GoTo Interpret_Error
    
            'if not existing, create new row
            If rowItem Is Nothing Then
                Set rowItem = tblItems.ListRows.Add
            End If
    
            'fill data
            On Error GoTo NextColumn
            For Each cl In tblItems.ListColumns
                Select Case TypeName(Item(cl.Name))
                    Case "Collection"
                        If Item(cl.Name).Count > 0 Then
                            ReDim subItemList(1 To Item(cl.Name).Count)
                            For i = 1 To Item(cl.Name).Count
                                subItemList(i) = Item(cl.Name)(i)
                            Next
                            rowItem.Range(cl.Index) = Join(subItemList, vbNewLine)
                        End If
                    Case "Dictionary"
                        Item(cl.Name).Add id_field, Item(id_field)
                        Set Col = New Collection
                        Col.Add Item(cl.Name)
                        Call Interpret(Col, tblName & "_" & cl.Name, id_field)
                    Case Else
                        rowItem.Range(cl.Index) = Item(cl.Name)
                        rowItem.Range(cl.Index).WrapText = False
                End Select
NextColumn:
            Next
            
        On Error GoTo Interpret_Error
        End If
    Next
    
    Interpret = True

    On Error GoTo 0
    Exit Function

Interpret_Error:

    Interpret = False
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in procedure Interpret of Module todoist_TODOist"

End Function

