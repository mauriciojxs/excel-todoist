Attribute VB_Name = "todoist_TODO_Internal"
''
' mdTODO_Internal v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Calls for data exchange between spreadsheet and local copies
' of TODOist databases, or calls to calls to add/change data in TODOist
'
' 1. Task routines
' 2. Users routines
' 3. Notes routines
' 4. Refresh and startup calls
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit
Option Private Module

' ============================================= '
' 1. Task routines
' ============================================= '
'
'
' Several routines to send / receive / update tasks
' from  / to TODOist
'
'
'


'
' Loop the tasks table and send updated info to TODOist
'
' @method SendTasks()
Sub SendTasks()

    Dim rowTask As ListRow
    Dim tblItems As ListObject
    Dim clItems As ListColumns
    Dim rowItem As ListRow
    Dim TaskLocal As TaskType
    Dim TaskRemote As TaskType
    Dim TasksToClose() As String
    Dim CloseCounter As Integer

    Set tblItems = Worksheets("InternalConfig").ListObjects("TblItems")
    Set clItems = tblItems.ListColumns
    
    ReDim TasksToClose(1 To cfg.Task.Table.ListRows.Count)

    'cycle the Tasks table
    For Each rowTask In cfg.Task.Table.ListRows
        TaskLocal.ID = rowTask.Range(cfg.Task.clID)

        'if task is already in TODOist
        If TaskLocal.ID <> "" Then

            'find the task in the internal table
            For Each rowItem In tblItems.ListRows

                If rowItem.Range(clItems.Item("id").Index) = TaskLocal.ID Then
                    Call FillTask(rowItem, TaskRemote, RemoteTask)
                    Call FillTask(rowTask, TaskLocal, LocalTask)
                    If Not TaskCompare(TaskLocal, TaskRemote) Then
                        'perform update
                        If TaskLocal.Closed Then
                            CloseCounter = CloseCounter + 1
                            TasksToClose(CloseCounter) = TaskLocal.ID
                        Else
                            Call UpdateTask(cfg, TaskLocal)
                        End If
                    Else
                        Exit For
                    End If
                End If
            Next
        End If
    Next
    
    If CloseCounter > 0 Then
        ReDim Preserve TasksToClose(1 To CloseCounter)
        Call CloseTasks(cfg, TasksToClose)
    End If

End Sub


'
' Retrieves all tasks from TODOist internal table to logbook
'
' @method RefreshTasks()
Sub RefreshTasks()

    Dim clItems As ListColumns
    Dim tblItems As ListObject
    Dim rowTask As ListRow
    Dim k As ListRow
    Dim TaskRemote As TaskType
    Dim labels() As String
    Dim i As Integer

    Set tblItems = Worksheets("InternalConfig").ListObjects("TblItems")
    Set clItems = tblItems.ListColumns

    'loop in internal table
    For Each k In tblItems.ListRows
        Call FillTask(k, TaskRemote, RemoteTask)
        'loop in local table
        For Each rowTask In cfg.Task.Table.ListRows
            If rowTask.Range(cfg.Task.clID) = TaskRemote.ID Then
                If TaskRemote.Priority <> 0 Then _
                    rowTask.Range(cfg.Task.clPriority) = TaskRemote.Priority
                If TaskRemote.DueDate <> 0 Then _
                    rowTask.Range(cfg.Task.clDueDate) = TaskRemote.DueDate
                rowTask.Range(cfg.Task.clResponsible) = TaskRemote.RespName
                If TaskRemote.Closed Then _
                    rowTask.Range(cfg.Task.clStatus) = cfg.Task.ClosedValue _
                Else _
                    rowTask.Range(cfg.Task.clStatus) = cfg.Task.OpenValue
                rowTask.Range(cfg.Task.clContent) = TaskRemote.Text
                ReDim labels(LBound(TaskRemote.label) To UBound(TaskRemote.label))
                For i = LBound(TaskRemote.label) To UBound(TaskRemote.label)
                    labels(i) = label(TaskRemote.label(i), LabelName)
                Next
                rowTask.Range(cfg.Task.clLabel) = Join(labels, vbNewLine)
                Exit For
            End If
        Next
    Next

End Sub


'
' Creates all tasks in TODOist when they are open
' and don't have an ID yet
'
' @ param tasks_todoist
Sub tasks_todoist()

    Dim rowTask As ListRow
    Dim Task_Prio As Integer
    Dim Task_Date As Date
    Dim TaskLocal As TaskType

    For Each rowTask In cfg.Task.Table.ListRows
        Call FillTask(rowTask, TaskLocal, LocalTask)
        If TaskLocal.ID = "" And TaskLocal.Text <> "" And Not TaskLocal.Closed Then
            rowTask.Range(cfg.Task.clID) = AddTask(cfg, TaskLocal)
        End If
    Next

End Sub


'
' Update all tasks of the project to apply the tag defined
' in the Setup
'
' @method RefreshTasks()
Sub UpdateTag()

    Dim rowTask As ListRow
    Dim isClosed As Boolean
    Dim Task As TaskType

    Call Init_TODOist(cfg)

    For Each rowTask In cfg.Task.Table.ListRows
        Call FillTask(rowTask, Task, LocalTask)
        Call UpdateTask(cfg, Task)
    Next

End Sub


'
' Compare the fields of two TaskType variables (except for the task
' text)
'
' @method TaskCompare
' @param {TaskType} First task
' @param {TaskType} Second task
' @return {Boolean} Tasks are equal
Function TaskCompare( _
    Task1 As TaskType, _
    Task2 As TaskType) _
    As Boolean
    
    Dim i As Integer

    If Task1.Closed = Task2.Closed And _
        Task1.DueDate = Task2.DueDate And _
        Task1.ID = Task2.ID And _
        Task1.Priority = Task2.Priority And _
        Task1.Text = Task2.Text And _
        Task1.RespName = Task2.RespName Then
        TaskCompare = True
    Else
        TaskCompare = False
        Exit Function
    End If
    
    If UBound(Task1.label) = UBound(Task2.label) Then
        For i = LBound(Task1.label) To UBound(Task1.label)
            If Task1.label(i) <> Task2.label(i) Then
                TaskCompare = False
                Exit Function
            End If
        Next
    Else
        TaskCompare = False
    End If

End Function

'
' Fills a TaskType variable with data from a table row.
' It is necessary to inform the type of the table (local
' or remote)
'
' @method FillTask
' @param {ListRow} Row of data for the task
' @param {TaskType} Task to be filled
' @param {LocalRemote} The type of task table (local: user table, remote: internal TODOist replica)
Sub FillTask( _
    ByRef row As ListRow, _
    ByRef Task As TaskType, _
    LocalOrRemote As LocalRemote)

    Dim clItems As ListColumns
    Dim i As Integer

    Select Case LocalOrRemote
        Case LocalRemote.RemoteTask
            Set clItems = Worksheets("InternalConfig").ListObjects("TblItems").ListColumns

            Task.ID = row.Range(clItems.Item("id").Index)
            If row.Range(clItems.Item("checked").Index) = "1" Or _
                row.Range(clItems.Item("in_history").Index) = "1" Then
                Task.Closed = True
            Else
                Task.Closed = False
            End If
            Task.DueDate = DateConv(row.Range(clItems.Item("due_date_utc").Index))
            Task.RespName = user(row.Range(clItems.Item("responsible_uid").Index), ID, Name)
            Task.Priority = ConvPriority(CInt(row.Range(clItems.Item("priority").Index)), FromTODOist)
            Task.Text = row.Range(clItems.Item("content").Index)
            Task.label = Split(row.Range(clItems.Item("labels").Index), Chr(10))

                

        Case LocalRemote.LocalTask
            Task.ID = row.Range(cfg.Task.clID)
            If row.Range(cfg.Task.clStatus) = cfg.Task.ClosedValue Then
                Task.Closed = True
            Else
                Task.Closed = False
            End If
            Task.DueDate = CDate(row.Range(cfg.Task.clDueDate))
            Task.RespName = row.Range(cfg.Task.clResponsible)
            Task.Priority = CInt(row.Range(cfg.Task.clPriority))
            Task.Text = row.Range(cfg.Task.clContent)
            Task.label = Split(row.Range(cfg.Task.clLabel), Chr(10))
            For i = LBound(Task.label) To UBound(Task.label)
                Task.label(i) = label(Task.label(i), LabelID)
            Next
            Task.Comments = Split(row.Range(cfg.Task.clCommentsID), Chr(10))

    End Select

End Sub


'
' Returns the label ID from the label name, or the
' label name from the ID
'
' @method Label
' @param {String} Label name or ID
' @param {LabelReturn} Type of return
' @return {String} The label name or ID, depending of the selection
Function label( _
    LabelStr As String, _
    ReturnType As LabelReturn _
    ) As String
    
    Dim tbl As ListObject
    Dim row As ListRow
    Dim inp As String
    Dim outp As String
    
    Set tbl = Sheets("InternalConfig").ListObjects("tblLabels")
    
    If ReturnType = LabelReturn.LabelID Then
        inp = "name"
        outp = "id"
    Else
        inp = "id"
        outp = "name"
    End If
    
    For Each row In tbl.ListRows
        If row.Range(tbl.ListColumns(inp).Index) = LabelStr Then
            label = row.Range(tbl.ListColumns(outp).Index)
            Exit Function
        End If
    Next

End Function

'
' Converts between local priority standard (1 = highest priority, 3 = lowest priority)
' and TODOist priority standard (1 = lowest priority, 4 = highest priority), in both
' directions
'
' @method ConvPriority
' @param {Integer} Priority to be converted
' @param {DirectionType} Conversion to TODOist or from TODOist standard
' @return {Integer} Converted priority
Function ConvPriority( _
    ByVal Priority As Integer, _
    Direction As DirectionType) _
    As Integer

    ConvPriority = 5 - Priority
    If Priority = 0 Then ConvPriority = 0
    If Direction = DirectionType.FromTODOist And Priority = 1 Then ConvPriority = 0

End Function


'
' Converts from TODOist textual date string to Excel
' date format. Discounts current user time offset defined
' in TODOist profile
' example input: Fri 26 Sep 2014 08:25:05 +0000
'
' @method DateConv
' @param {String} TODOist text string representing a date
' @return {Date} Converted from input and discounted time offset
Function DateConv( _
    ByVal date_str As String) _
    As Date

    Dim day As String
    Dim month_str As String
    Dim month As String
    Dim year As String
    Dim hour As String
    Dim tz_offset As Integer
    Dim tblUserInfo As ListObject

    day = Mid(date_str, 5, 2)
    month_str = Mid(date_str, 8, 3)
    year = Mid(date_str, 12, 4)
    hour = Mid(date_str, 17, 8)

    Set tblUserInfo = Sheets("InternalConfig").ListObjects("TblUserInfo")
    tz_offset = tblUserInfo.ListRows(1).Range(tblUserInfo.ListColumns.Item("tz_offset").Index)

    Select Case month_str
        Case "Jan"
            month = "01"
        Case "Feb"
            month = "02"
        Case "Mar"
            month = "03"
        Case "Apr"
            month = "04"
        Case "May"
            month = "05"
        Case "Jun"
            month = "06"
        Case "Jul"
            month = "07"
        Case "Aug"
            month = "08"
        Case "Sep"
            month = "09"
        Case "Oct"
            month = "10"
        Case "Nov"
            month = "11"
        Case "Dec"
            month = "12"
    End Select

    If year = "" Or month = "" Or day = "" Then
        DateConv = 0
    Else
        DateConv = Int(DateAdd("h", tz_offset, CDate(year & "-" & month & "-" & day & " " & hour)))
    End If

End Function


' ============================================= '
' 2. Users routines
' ============================================= '
'
'
' Several routines to update users and get user
' information
'
'

'
' Looks for all people invited to share the project
'
' @method RefreshUsers()
Sub RefreshUsers()

    Dim rowPerson As ListRow
    Dim row As ListRow
    Dim tblStatus As ListObject
    Dim tblCollab As ListObject
    Dim rowPersons As ListRows
    Dim clPerson As ListColumns
    Dim user_id As String
    
    Set tblStatus = Sheets("InternalConfig").ListObjects("TblCollaboratorStates")
    Set tblCollab = Sheets("InternalConfig").ListObjects("TblCollaborators")
    Set rowPersons = cfg.People.Table.ListRows
    
    cfg.People.Table.Parent.Unprotect
    
    On Error Resume Next
    cfg.People.Table.DataBodyRange.Delete
    On Error GoTo 0

    For Each row In tblStatus.ListRows
        If row.Range(tblStatus.ListColumns("project_id").Index) = cfg.Project.ProjectID And _
            row.Range(tblStatus.ListColumns("is_deleted").Index) = False Then
            'good user
            'get id
            user_id = row.Range(tblStatus.ListColumns("user_id").Index)
            Set rowPerson = rowPersons.Add
            rowPerson.Range(cfg.People.clID) = user_id
            'find name and email
            rowPerson.Range(cfg.People.clName) = user(user_id, ID, Name)
            rowPerson.Range(cfg.People.clEmail) = user(user_id, ID, email)
            rowPerson.Range(cfg.People.clState) = row.Range(tblStatus.ListColumns("state").Index)
        End If
    Next
    
    cfg.People.Table.Parent.Protect DrawingObjects:=True, Contents:=True, Scenarios:=True

End Sub


'
' Return user info from any input type to any output type
' Valid types are according to ParamType:
' - email
' - name
' - id
'
' @method user()
' @param {String} Input (user name, ID or email)
' @param {ParamType} Input Type
' @param {ParamType = ID} Output Type
Function user( _
    ByVal input_value As String, _
    ByVal input_type As ParamType, _
    Optional ByVal output_type As ParamType = ParamType.ID) _
    As String

    Dim tblCollab As ListObject
    Dim rowUser As ListRow
    Dim user_id As String
    Dim user_email As String
    Dim user_name As String

    Set tblCollab = Sheets("InternalConfig").ListObjects("TblCollaborators")

    input_value = Trim(input_value)
    
    If input_value = "" Then
        user = vbNullString
        Exit Function
    End If

    Select Case input_type
        Case ParamType.email
            For Each rowUser In tblCollab.ListRows
                If rowUser.Range(tblCollab.ListColumns("email").Index) = input_value Then
                    user_id = rowUser.Range(tblCollab.ListColumns("id").Index)
                    user_email = input_value
                    user_name = rowUser.Range(tblCollab.ListColumns("full_name").Index)
                    Exit For
                End If
            Next
        Case ParamType.Name
            For Each rowUser In cfg.People.Table.ListRows
                If rowUser.Range(cfg.People.clName) = input_value Then
                    user_id = rowUser.Range(cfg.People.clID)
                    user_email = rowUser.Range(cfg.People.clEmail)
                    user_name = input_value
                    Exit For
                End If
            Next
        Case ParamType.ID
            For Each rowUser In tblCollab.ListRows
                If rowUser.Range(tblCollab.ListColumns("id").Index) = input_value Then
                    user_email = rowUser.Range(tblCollab.ListColumns("email").Index)
                    user_id = input_value
                    user_name = rowUser.Range(tblCollab.ListColumns("full_name").Index)
                    Exit For
                End If
            Next
        Case Else
            user = vbNullString
            Exit Function
    End Select

    Select Case output_type
        Case ParamType.email
            user = user_email
        Case ParamType.ID
            user = user_id
        Case ParamType.Name
            user = user_name
        Case Else
            user = vbNullString
            Exit Function
    End Select

End Function

' ============================================= '
' 3. Notes routines
' ============================================= '
'
'
' Several routines to send / receive / update tasks
' from  / to TODOist
'
'

'
' Split a string formed of one to several task notes formatted
' according to a standard. Notes are updated in the provided array.
' If a unformatted note is found in the end of the string, it is
' considered a new note, added to TODOist and formatted in the array.
' If a note is marked as deleted in TODOist, it is deleted from the
' array. If a note is added in TODOist, it is included in the array.
' Standard note format:
' [YYYY-MM-DD: note text, one to several lines]
'
' @method SplitNotes
' @param {String} Input notes string
' @param {String} String of notes IDs
' @param {Notes()} Array of notes to be returned
' @param {TaskType} Task the notes belong to
Sub SplitNotes( _
    ByVal NotesStr As String, _
    ByVal IDStr As String, _
    ByRef Notes() As String, _
    ByRef TaskLocal As TaskType)

    Dim tmpArray() As Variant
    Dim tmpID() As Variant
    Dim tmpNote() As String
    Dim a As Integer
    Dim b As Integer
    Dim arrayMax As Long
    Dim IDMax As Long
    Dim counter As Long
    Dim tblNotes As ListObject
    Dim clNotes As ListColumns
    Dim rowNote As ListRow
    Dim found As Boolean

    Set tblNotes = Sheets("InternalConfig").ListObjects("TblNotes")
    Set clNotes = tblNotes.ListColumns

    tmpArray = SplitBase1(NotesStr, "]")
    tmpID = SplitBase1(IDStr, Chr(10))

    If IsArrayAllocated(tmpArray) Then
        arrayMax = UBound(tmpArray)
        Do While tmpArray(arrayMax) = ""
            arrayMax = arrayMax - 1
        Loop
    Else
        arrayMax = 0
    End If

    If IsArrayAllocated(tmpID) Then
        IDMax = UBound(tmpID)
    Else
        IDMax = 0
    End If

    arrayMax = StringAdjust(tmpArray, arrayMax)

    If IDMax < arrayMax Then
        ReDim Preserve tmpID(1 To arrayMax)
        tmpID(arrayMax) = ""
    End If

    ReDim Notes(1 To 3, 1 To (arrayMax + tblNotes.ListRows.Count))

    'add note to TODOist and/or properly format
    For a = 1 To arrayMax
        tmpNote = FormatNote(tmpArray(a), tmpID(a), TaskLocal)
        Notes(1, a) = tmpNote(1) 'id
        Notes(2, a) = tmpNote(2) 'date
        Notes(3, a) = tmpNote(3) 'note
    Next

    'include notes in local table not yet in the array
    counter = arrayMax
    For Each rowNote In tblNotes.ListRows
        'check if note belongs to task
        If rowNote.Range(clNotes.Item("item_id").Index) = TaskLocal.ID Then
            found = False

            'check initial range if note was not already included
            a = 1
            Do While a <= arrayMax
                If rowNote.Range(clNotes.Item("id").Index) = Notes(1, a) Then
                    found = True
                    'check if note was deleted in TODOist
                    If rowNote.Range(clNotes.Item("is_deleted").Index) = "1" Or _
                        rowNote.Range(clNotes.Item("is_archived").Index) = "1" Then
                        'delete
                        If a < arrayMax Then
                            'not in the end of the array
                            For b = a + 1 To arrayMax
                                Notes(1, b - 1) = Notes(1, b)
                                Notes(2, b - 1) = Notes(2, b)
                                Notes(3, b - 1) = Notes(3, b)
                            Next
                        End If
                        arrayMax = arrayMax - 1
                        counter = counter - 1
                    End If
                    Exit Do
                End If
                a = a + 1
            Loop
            'if not found and not deleted
            If Not found And _
                rowNote.Range(clNotes.Item("is_deleted").Index) = "0" And _
                rowNote.Range(clNotes.Item("is_archived").Index) = "0" Then
                'add
                counter = counter + 1
                Notes(1, counter) = rowNote.Range(clNotes.Item("id").Index)
                Notes(2, counter) = Format(DateConv(rowNote.Range(clNotes.Item("posted").Index)), "yyyy-mm-dd")
                Notes(3, counter) = rowNote.Range(clNotes.Item("content").Index)
            End If
        End If
    Next

    If counter > 0 Then
        If counter <> UBound(Notes, 2) Then ReDim Preserve Notes(1 To 3, 1 To counter)
    Else
        ReDim Notes(0, 0)
    End If

End Sub

'
' Corrects the position if a new note is added somewhere that not
' after all existing notes
'
' @method StringAdjust
' @param {Variant()} Input notes array
' @param {Integer} upper bound of the array (may be different than ubound)
' @return {Integer} New upper bound of the array
Function StringAdjust( _
    ByRef Notes() As Variant, _
    ByVal arrMax As Integer) _
    As Integer

    Dim i As Integer
    Dim Note As String
    Dim tmpStr() As Variant
    Dim iMin As Integer
    Dim iMax As Integer

    For i = 1 To arrMax
        Note = Notes(i)
        If Right(Note, 1) = Chr(10) Then Note = Left(Note, Len(Note) - 1)
        If Left(Note, 1) = Chr(10) Then Note = Right(Note, Len(Note) - 1)
        tmpStr = SplitBase1(Note, "[")
        If IsArrayAllocated(tmpStr) Then
            iMin = LBound(tmpStr)
            iMax = UBound(tmpStr)
            Do While tmpStr(iMin) = ""
                iMin = iMin + 1
            Loop
            If iMin <> iMax Then
                'move to last position
                If arrMax >= UBound(Notes) Then ReDim Preserve Notes(1 To arrMax + 1)
                arrMax = arrMax + 1
                Notes(arrMax) = tmpStr(iMin)
                'replace current position with the "right" part
                Notes(i) = "[" & tmpStr(iMax)
            End If
        End If
    Next

    StringAdjust = arrMax

End Function



'
' Check for default formating in Note string and
' return an array with id, date and note. If string
' is not formatted as per the standard, it is considered
' a new note and added to TODOist.
' Standard format is
' [yyyy-mm-dd: notes text]
'
' @method FormatNote
' @param {String} Input string with notes
' @param {String} input string with ID
' @param {TaskType} Task the note belongs to
' @return {String()} Array with note ID(1), date(2) and text(3)
Function FormatNote( _
    ByVal strInput As String, _
    ByVal strID As String, _
    ByRef TaskLocal As TaskType) _
    As String()

    Dim tmpString(0 To 2) As String
    Dim str1 As String
    Dim str2 As String
    Dim str3 As String
    Dim Result(1 To 3) As String
    ReDim FormatNote(1 To 3) As String

    If strInput = "" Then Exit Function

    If Left(strInput, 1) = Chr(10) Then
        strInput = Right(strInput, Len(strInput) - 1)
    End If

    If Right(strInput, 1) = Chr(10) Then
        strInput = Left(strInput, Len(strInput) - 1)
    End If

    tmpString(0) = strID
    tmpString(1) = Right(Left(strInput, 11), 10)
    tmpString(2) = Right(strInput, WorksheetFunction.max(Len(strInput) - 13, 0))

    If IsDate(tmpString(1)) And Left(strInput, 1) = "[" Then
        str1 = tmpString(0)
        str2 = tmpString(1)
        str3 = tmpString(2)
    Else
        str1 = AddNote(cfg, strInput, TaskLocal.ID)
        str2 = Format(Date, "yyyy-mm-dd")
        str3 = strInput
    End If

    Result(1) = str1
    Result(2) = str2
    Result(3) = Trim(str3)

    FormatNote = Result()

End Function

'
' Loop the table of tasks and add notes to TODOist
' and/or properly format existing notes
'
' @method ProcessNotes()
Sub ProcessNotes()

    Dim Notes() As String
    Dim str As String
    Dim a As Integer
    Dim rowTask As ListRow
    Dim textLen() As Integer
    Dim TaskLocal As TaskType

    Application.ScreenUpdating = False

    For Each rowTask In cfg.Task.Table.ListRows
        Call FillTask(rowTask, TaskLocal, LocalTask)
        If TaskLocal.ID <> "" Then
            Call SplitNotes(rowTask.Range(cfg.Task.clComments), _
            rowTask.Range(cfg.Task.clCommentsID), _
            Notes(), TaskLocal)
            If UBound(Notes, 2) > 0 Then
                ReDim textLen(1 To UBound(Notes, 2))
                ReDim dateLen(1 To UBound(Notes, 2))
                ReDim firstPos(1 To UBound(Notes, 2))
                ReDim lastPos(1 To UBound(Notes, 2))
                rowTask.Range(cfg.Task.clComments) = ""
                rowTask.Range(cfg.Task.clCommentsID) = ""
                For a = 1 To UBound(Notes, 2)
                    textLen(a) = Len(rowTask.Range(cfg.Task.clComments)) + 1
                    rowTask.Range(cfg.Task.clComments) = _
                    rowTask.Range(cfg.Task.clComments) & _
                    Chr(10) & "[" & Notes(2, a) & ": " & Notes(3, a) & "]"
                    rowTask.Range(cfg.Task.clCommentsID) = _
                    rowTask.Range(cfg.Task.clCommentsID) & _
                    Chr(10) & Notes(1, a)
                Next
                rowTask.Range(cfg.Task.clComments) = _
                Right(rowTask.Range(cfg.Task.clComments), _
                Len(rowTask.Range(cfg.Task.clComments)) - 1)
                rowTask.Range(cfg.Task.clCommentsID) = _
                Right(rowTask.Range(cfg.Task.clCommentsID), _
                Len(rowTask.Range(cfg.Task.clCommentsID)) - 1)
                For a = 1 To UBound(Notes, 2)
                    rowTask.Range(cfg.Task.clComments).Characters( _
                    textLen(a) + 1, 11).Font.Bold = True
                Next
            End If
        End If
    Next

    Application.ScreenUpdating = True

End Sub


' ============================================= '
' 4. Refresh and startup calls
' ============================================= '
'
'
' Routines to updates all data from / to TODOist
' and to refresh project/label data for the initial
' workbook setup
'
'

'
' Refreshes the spreadsheet with TODOist data
'
' @method From_TODO_sub
Sub From_TODO_sub()

    Call Init_TODOist(cfg)
    If cfg.Project.ProjectID = "" Then
        Call MsgBox("To set a project ID in TODOist, please go to the Setup tab", vbCritical, "TODOist not set")
        Exit Sub
    End If

    Application.StatusBar = "Running..."

    'refresh local table
    Call GetTODOist(cfg)

    'process task updates
    Call RefreshTasks

    'process user updates
    Call RefreshUsers

    'format existing notes and add new ones
    Call ProcessNotes

    Application.StatusBar = "Refresh done!"

    Application.OnTime Now + TimeSerial(0, 0, 10), "ClearStatusBar"

End Sub


'
' Refreshes TODOist with spreadsheet data
'
' @method To_TODO_sub
Sub To_TODO_sub(Optional Exiting As Boolean = False)
   
    'Call Init_TODOist(cfg)
    If cfg.Project.ProjectID = "" Then
        Call MsgBox("To set a project ID in TODOist, please go to the Setup tab", vbCritical, "TODOist not set")
        Exit Sub
    End If

    Application.StatusBar = "Running..."

    'refresh local table
    Call GetTODOist(cfg)

    'refresh user IDs
    Call RefreshUsers

    'put new tasks to TODOist
    Call tasks_todoist

    'refresh local Tasks table
    Call GetTasks(cfg)

    'put updates to TODOist
    Call SendTasks

    'refresh local notes table
    Call GetNotes(cfg)

    'format existing notes and add new ones
    Call ProcessNotes

    Application.StatusBar = "Refresh done!"

    If Exiting Then
        Application.Wait Now + TimeSerial(0, 0, 1)
        Call ClearStatusBar
        Application.Wait Now + TimeSerial(0, 0, 1)
    Else
        Application.OnTime Now + TimeSerial(0, 0, 10), "ClearStatusBar"
    End If

End Sub

'
' Gives the control of the status bar back to Excel
'
'
' @method ClearStatusBar
Sub ClearStatusBar()

    Application.StatusBar = False

End Sub


'
' Updates the project list and labels list, even
' before initializing the client. Used for the
' initial setup of the workbook
'
' @method update_project_list
Public Sub update_project_list()

    Call Init_TODOist(cfg)
    Call GetProjects(cfg)
    Call GetLabels(cfg)
    Call MsgBox("List refreshed. Choose the project and label accordingly on table below.", vbInformation, "List refreshed")

End Sub


'---------------------------------------------------------------------------------------
' Procedure  : ClearTables
' Purpose    : Used to clear all data of the workbook before distributting
'
' Parameters :
' Return     : []
'---------------------------------------------------------------------------------------
Sub ClearTables()
Dim tbl As ListObject
Dim ws As Worksheet

On Error Resume Next

For Each ws In ThisWorkbook.Worksheets
    WebHelpers.LogDebug "Entering sheet " & ws.Name & "...", "ClearTables"
    For Each tbl In ws.ListObjects
        Select Case tbl.Name
            Case "TblPrjID", "TblCfgTasks_Active", "TblCfgTasks", "TblCfgPeople_Active", "TblCfgPeople", _
                "TblAppSettings"
                WebHelpers.LogDebug Space(3) & "Skipping table " & tbl.Name & "...", "ClearTables"
            Case "TblSeq"
                WebHelpers.LogDebug Space(3) & "Adjusting table " & tbl.Name & "...", "ClearTables"
                tbl.ListColumns(2).DataBodyRange.Clear
                tbl.ListColumns(3).DataBodyRange.Clear
            Case "TblSetup"
                WebHelpers.LogDebug Space(3) & "Clearing table " & tbl.Name & "...", "ClearTables"
                tbl.DataBodyRange.ClearContents
            Case "TblTasks"
                WebHelpers.LogDebug Space(3) & "Clearing table " & tbl.Name & "...", "ClearTables"
                tbl.DataBodyRange.Offset(1).Resize(tbl.ListRows.Count - 1, tbl.ListColumns.Count).Rows.Delete
                tbl.DataBodyRange.ClearContents
            Case "TblNames"
                WebHelpers.LogDebug Space(3) & "Unprotecting, deleting contents and protecting table " & tbl.Name & "...", "ClearTables"
                tbl.Parent.Unprotect
                tbl.DataBodyRange.Delete
                tbl.Parent.Protect DrawingObjects:=True, Contents:=True, Scenarios:=True
            Case Else
                WebHelpers.LogDebug Space(3) & "Deleting contents for table " & tbl.Name & "...", "ClearTables"
                tbl.DataBodyRange.Delete
        End Select
    Next
Next
WebHelpers.LogDebug "Done", "ClearTables"

On Error GoTo 0

End Sub



'---------------------------------------------------------------------------------------
' Procedure  : ProcessInvitations
' Purpose    : Goes through the TblLiveNotifications table and process any open
'              invitation to share a project or join a business account
'
' Parameters :
' Return     : [Boolean] There were invitations to process
'---------------------------------------------------------------------------------------
Function ProcessInvitations() As Boolean

Dim row As ListRow
Dim tbl As ListObject
Dim username As String
Dim usermail As String
Dim Project As String
Dim InvitationID As String
Dim InvitationSecret As String
Dim tblFrom As ListObject
Dim rowFrom As ListRow
Dim msg As String
Dim msg_user As String
Dim biz As Boolean

    ProcessInvitations = False

    On Error GoTo ProcessInvitations_Error
    
    Set tbl = Sheets("InternalConfig").ListObjects("TblLiveNotifications")
    Set tblFrom = Sheets("InternalConfig").ListObjects("TblLiveNotifications_from_user")
    
    For Each row In tbl.ListRows
        If (row.Range(tbl.ListColumns("notification_type").Index) = "share_invitation_sent" Or _
            row.Range(tbl.ListColumns("notification_type").Index) = "biz_invitation_created") And _
            row.Range(tbl.ListColumns("state").Index) = "invited" Then
                
            ProcessInvitations = True
            biz = (row.Range(tbl.ListColumns("notification_type").Index) = "biz_invitation_created")
            
            'get user
                For Each rowFrom In tblFrom.ListRows
                    If rowFrom.Range(tblFrom.ListColumns("seq_no").Index) = _
                        row.Range(tbl.ListColumns("seq_no").Index) Then
                        username = rowFrom.Range(tblFrom.ListColumns("full_name").Index)
                        usermail = rowFrom.Range(tblFrom.ListColumns("email").Index)
                        Exit For
                    End If
                Next
            'get project
                If Not biz Then
                    Project = row.Range(tbl.ListColumns("project_name").Index)
                    msg_user = "User " & username & " (" & usermail & ") wants to share a project called """ & _
                        Project & """ with you. Do you accept?"
                Else
                    Project = row.Range(tbl.ListColumns("account_name").Index)
                    msg = row.Range(tbl.ListColumns("invitation_message").Index)
                    msg_user = "User " & username & " (" & usermail & ") invites you to a business account of company """ & _
                        Project & """, with the message: " & msg & vbNewLine & "Do you accept?"
                End If
            'get invitation info
                InvitationID = row.Range(tbl.ListColumns("invitation_id").Index)
                InvitationSecret = row.Range(tbl.ListColumns("invitation_secret").Index)
            'confirm with user
            Select Case MsgBox(msg_user, vbYesNoCancel Or vbInformation Or vbDefaultButton1, _
                "Invitation received")
                Case vbYes
                    'accept share
                    Call ReplyInvitation(cfg, InvitationID, InvitationSecret, True, biz)
                Case vbNo
                    'reject share
                    Call ReplyInvitation(cfg, InvitationID, InvitationSecret, False, biz)
                Case vbCancel
                    'do nothing
            End Select
        End If
    Next

    On Error GoTo 0
    Exit Function

ProcessInvitations_Error:

    ProcessInvitations = False
    MsgBox "Error " & Err.Number & " (" & Err.Description & _
        ") in procedure ProcessInvitations of Module todoist_TODOist"
End Function

'---------------------------------------------------------------------------------------
' Procedure  : GetToken
' Purpose    : Retrieve the Token in the Authenticator object
'
' Parameters : [TodoistAuthenticator] Object holding auth info
' Return     : [String] The token (if any)
'---------------------------------------------------------------------------------------
Function GetToken(Auth As TodoistAuthenticator) As String

    On Error GoTo GetToken_Error

    GetToken = Auth.Token

    On Error GoTo 0
    Exit Function

GetToken_Error:

    GetToken = ""

End Function



'---------------------------------------------------------------------------------------
' Procedure  : Logoff_sub
' Purpose    : Logoff the current user on TODOist
'
' Parameters : [TodoistAuthenticator] Client holding authentication info
' Return     : []
'---------------------------------------------------------------------------------------
Sub Logoff_sub(Auth As TodoistAuthenticator)

    On Error GoTo Logoff_sub_Error

    Auth.Logout

    On Error GoTo 0
    Exit Sub

Logoff_sub_Error:

End Sub
