Attribute VB_Name = "mdGUID"
''
' mdGUID v1.0
' (c) Mauricio Souza - https://bitbucket.org/mauriciojxs/Excel-TODOist/
'
' Generates UUID/GUID code as per Microsoft implementation
' Adapted from code found at
' http://stackoverflow.com/questions/7031347/how-can-i-generate-guids-in-excel
'
' 1. Generate GUID/UUID
'
' @author: mauriciojxs@yahoo.com.br
' @license: MIT (http://www.opensource.org/licenses/mit-license.php
'
' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ '

Option Explicit
Option Private Module

' ============================================= '
' 1. Generate GUID/UUID
' ============================================= '
'
'
' Called from other subs/functions or cell formula.
' Returns a string with a GUID/UUID.
' Format is 24DD18D4-C902-497F-A64B-28B2FA741661
'
'
' @method GenGuid()
' @return {String} GUID/UUID
Public Function GenGuid() As String

    Dim TypeLib As Object
    Dim Guid As String

    Set TypeLib = CreateObject("Scriptlet.TypeLib")
    Guid = TypeLib.Guid

    Guid = Replace(Guid, "{", "")
    Guid = Replace(Guid, "}", "")
    GenGuid = Left(Guid, 36)

End Function
